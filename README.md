# GHeap

A Heap/PriorityQueue for GLib.

A rather simple heap implementation.
You could make this faster by doing the whole thing as macros, but that is rather difficult to maintain.
So this is the result of cleaning things up.

```c
static int
cmpint (gconstpointer a,
        gconstpointer b)
{
  return *(const gint *)a - *(const gint *)b;
}

int
main (gint   argc,
      gchar *argv[])
{
  GHeap *heap;
  gint i;
  gint v;

  heap = g_heap_new (sizeof (gint), cmpint);

  for (i = 0; i < 10000; i++)
    g_heap_insert_val (heap, i);
  for (i = 0; i < 10000; i++)
    g_heap_extract (heap, &v);

  g_heap_unref (heap);

  return 0;
}
```
