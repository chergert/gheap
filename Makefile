all: test-gheap

WARNINGS = -Wall
DEBUG = -ggdb
OPTIMIZE = -O2 -mtune=native
CFLAGS = $(shell pkg-config --cflags $(PKGS))
LIBS = $(shell pkg-config --libs $(PKGS))
PKGS = glib-2.0

test-gheap: gheap.c gheap.h test-gheap.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(CFLAGS) $(LIBS) gheap.c test-gheap.c

clean:
	rm -f test-gheap
